import {StyleSheet, TextInput, View, Button} from "react-native";
import React from "react";

export default function SearchBlock() {
    return (
        <View style={styles.searchBlock}>
            <TextInput
                style={styles.searchInput}
                placeholder='search contact'
            />
            <Button
                style={styles.buttonSearch}
                title={
                    "Search"
                }
            />
        </View>
    );
}

const styles = StyleSheet.create({
    searchBlock: {
        width: '100%',
        flexDirection: 'row',
        paddingHorizontal: 20,
        paddingVertical: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },

    searchInput: {
        height: 30,
        paddingVertical: 8,
        paddingHorizontal: 8,
        flex: 1,
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: '#000'
    },
    buttonSearch:{
        backgroundColor: '#000',
        height: 100
    }
});