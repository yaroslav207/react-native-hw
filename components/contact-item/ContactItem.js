import {StyleSheet, TextInput, View, Button, Image, Text} from "react-native";
import React from "react";

export default function ContactItem({contactInfo, onPressInfoButton}) {
    console.log(contactInfo);
    return (
        <View style={styles.contactItem}>
            <View>
                <Text>{contactInfo.name}</Text>
                {contactInfo?.phoneNumbers && contactInfo?.phoneNumbers[0].number
                    ? <Text>{contactInfo?.phoneNumbers[0].number}</Text>
                    : null
                }
            </View>
            <Button title='info' onPress={() => onPressInfoButton(contactInfo.id)}/>
        </View>
    );
}

const styles = StyleSheet.create({
    contactItem: {
        width: '100%',
        flexDirection: 'row',
        paddingHorizontal: 5,
        paddingVertical: 10,
        alignItems: 'center',
        justifyContent: "space-between",
        borderStyle: 'solid',
        borderBottomColor: '#444',
        borderBottomWidth: 1,
    }
});