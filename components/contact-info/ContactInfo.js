import React, {useEffect, useState} from "react";
import * as Contacts from "expo-contacts";
import Communications from 'react-native-communications';

import {StyleSheet, TextInput, View, Button, Text, TouchableOpacity} from "react-native";

export default function ContactInfo({contactId}) {
    const [contact, setContact] = useState(null);

    useEffect(() => {
        (async () => {
            const {status} = await Contacts.requestPermissionsAsync();
            if (status === 'granted') {
                const data = await Contacts.getContactByIdAsync(contactId);
                setContact(data)
            }
        })();
    }, [contactId]);


    return contact
        ? (<View style={styles.container}>
            <Text style={styles.phoneContainer}>Name: {contact.name}</Text>
            {contact.phoneNumbers &&
            contact.phoneNumbers.map((item, index) => (<View style={styles.phoneContainer}>
                    <Text>Phone number {index + 1}: {item.number}</Text>
                    <Button onPress={() => Communications.phonecall(item.number, true)} title='Call'/>
                </View>
            ))
            }
        </View>)
        : (<Text>Loading...</Text>);
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 20,
        paddingVertical: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    phoneContainer: {
        height: 40,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    }
});