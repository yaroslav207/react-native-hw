import {StyleSheet, TextInput, View, Button, ScrollView} from "react-native";
import React, {useState, useEffect} from "react";
import * as Contacts from 'expo-contacts';

import ContactItem from "../contact-item/ContactItem"
import SearchBlock from "../search-block/SearchBlock";


const contactsArray = [
    {
        id: 321,
        name: 'Вася',
        phoneNumbers: [{
            number: '+380954457483'
        }],
        avatar: ''
    },
    {
        id: 123,
        name: 'Slava',
        phoneNumbers: [{
            number: '+380954457483'
        }],
        avatar: ''
    },
    {
        id: 231,
        name: 'Maks',
        phoneNumbers: [{
            number: '+380954457443'
        }],
        avatar: ''
    }
];

export default function ContactList({showInfoContact, navigation}) {

    const [contacts, setContacts] = useState(contactsArray);

    const onPressInfoButton = (id) => {
        showInfoContact(id);
        navigation.navigate('DetailsContact');
    };

    useEffect(() => {
        (async () => {
            const {status} = await Contacts.requestPermissionsAsync();
            if (status === 'granted') {
                const {data} = await Contacts.getContactsAsync({
                    fields: [Contacts.Fields.PhoneNumbers],
                });

                setContacts(data)
            }
        })();
    }, []);

    console.log(contacts);
    return (
        <View style={styles.container}>
            <SearchBlock/>
            <ScrollView style={styles.contactList}>
                {contacts.map(contact => (
                    <ContactItem
                        key={contact.id}
                        contactInfo={contact}
                        onPressInfoButton={onPressInfoButton}
                    />
                ))}
            </ScrollView>
        </View>

    );
}

const styles = StyleSheet.create({
    container: {
        paddingVertical: 30,
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    contactList: {
        flex: 1,
        width: '100%',
        paddingHorizontal: 20,
        paddingVertical: 10,
    }
});
