import SearchBlock from './search-block/SearchBlock';
import ContactList from './contact-list/ContactList';
import ContactInfo from './contact-info/ContactInfo'

export {
    SearchBlock,
    ContactList,
    ContactInfo
}

