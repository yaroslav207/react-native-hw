import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import {ContactList, ContactInfo} from './components/index'


const Stack = createStackNavigator();

export default function App() {
    const [selectContact, setSelectContact] = useState(null);

    const showInfoContact = (id) => {
        setSelectContact(id)
    };

  return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Home">
          <Stack.Screen name="Home" component={
              (props) => <ContactList
                  {...props}
                  showInfoContact={showInfoContact}
              />
          } />
          <Stack.Screen name="DetailsContact" component={
              (props) => <ContactInfo
                  {...props}
                  contactId={selectContact}
              />
          } />
        </Stack.Navigator>
      </NavigationContainer>

  );
}
